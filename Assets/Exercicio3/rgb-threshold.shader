Shader "Custom/threshold" { //ShaderLab code
	Properties {	
		_MainTexture ("Main Texture", 2D) = "white" {} //Texture map
		_threshold ("Threshold", Range(0.0,10.0)) = 0.0
	}

	SubShader {
		Pass {
				CGPROGRAM
				#pragma vertex vert;
				#pragma fragment frag;
				#include "UnityCG.cginc"

				float _threshold;
				uniform sampler2D _MainTexture;

				struct VertexInput
				{
					float4 vertex : POSITION;
						float4 texCoord : TEXCOORD0;
				};

				
				struct VertexOutput{
					float4 pos : SV_POSITION;
					float2 texCoord : TEXCOORD0;
				};

				VertexOutput vert (VertexInput input) {
					VertexOutput output;
					output.pos = UnityObjectToClipPos(input.vertex);
					output.texCoord = input.texCoord;
					return output;
				}

				
				fixed4 frag (VertexOutput input) : COLOR{
					float4 textureColour = tex2D(_MainTexture,input.texCoord.xy);
					float grayColor = textureColour.r*0.3+textureColour.g+0.59+textureColour.b*0.11;
					
					if (grayColor > _threshold){grayColor=1.0;}
					else{grayColor=0.0;}

					//Otherwise, if we get here just return tinted colour from our main texture
					return grayColor;
				}
				ENDCG
		}
	}
}
Shader "Custom/HSVToRGB"
{
    Properties
    {
        _ColorH ("H", Range(0,360)) = 0.0
	_ColorS ("S", Range(0.0,100.0)) = 0.0
	_ColorV ("V", Range(0.0,100.0)) = 0.0

        _X ("Size X",Float) = 1.0
        _Y ("Size Y",Float) = 1.0
    }
    SubShader
    {   
    Pass
    {
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        float4 _Color;
        float _ColorH ;
	float _ColorS ;
	float _ColorV ;
        float _X;
        float _Y;

        float4 vert(float4 vertexPos:POSITION) :SV_POSITION
        {
            return UnityObjectToClipPos(float4(_X,_Y,1,1)*
            vertexPos);
        }
        float4 frag() :Color
        {

    	  float c = (_ColorV/100) * (_ColorS/100);
   	  float x = c * (1 - abs(fmod(_ColorH/60, 2) - 1));
    	  float m = (_ColorV/100) - c;


	  float r = 0;
    	  float g = 0;
    	  float b = 0;
	
	  if (_ColorH >= 0 && _ColorH < 60) {
        	r = c;
        	g = x;
        	b = 0;
    		} 
          else if (_ColorH >= 60 && _ColorH < 120) {
        	r = x;
        	g = c;
        	b = 0;
    		} 
          else if (_ColorH >= 120 && _ColorH < 180) {
        	r = 0;
        	g = c;
        	b = x;
    		} 
          else if (_ColorH >= 180 && _ColorH < 240) {
        	r = 0;
        	g = x;
        	b = c;
    		} 
          else if (_ColorH >= 240 && _ColorH < 300) {
        	r = x;
        	g = 0;
        	b = c;
    		} 
          else if (_ColorH >= 300 && _ColorH < 360) {
        	r = c;
        	g = 0;
        	b = x;
    		}

  	  r += m;
  	  g += m;
  	  b += m;
	  _Color = float4(r,g,b,1);
	
            return _Color;
        }

        ENDCG
    }    
    }
    FallBack "Diffuse"
}

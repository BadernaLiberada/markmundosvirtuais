Shader "Custom/CMYKToRGB"
{
    Properties
    {
        _ColorC ("Cyan", Range(0.0,100.0)) = 0.0
	_ColorM ("Magenta", Range(0.0,100.0)) = 0.0
	_ColorY ("Yellow", Range(0.0,100.0)) = 0.0
	_ColorK ("Black", Range(0.0,100.0)) = 0.0
        _X ("Size X",Float) = 1.0
        _Y ("Size Y",Float) = 1.0
    }
    SubShader
    {   
    Pass
    {
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        float4 _Color;
        float _ColorC;
	float _ColorM;
	float _ColorY;
	float _ColorK;
        float _X;
        float _Y;

        float4 vert(float4 vertexPos:POSITION) :SV_POSITION
        {
            return UnityObjectToClipPos(float4(_X,_Y,1,1)*
            vertexPos);
        }
        float4 frag() :Color
        {
		_ColorC = _ColorC/100.0;
		_ColorM = _ColorM/100.0;
		_ColorY = _ColorY/100.0;
		_ColorK = _ColorK/100.0;

            float _R  = 255 * (1-_ColorC) * (1-_ColorK);
	    float _G = 255 * (1-_ColorM) * (1-_ColorK);
	    float _B = 255 * (1-_ColorY) * (1-_ColorK);
	    _Color = float4(_R,_G,_B,1);
            return _Color;
        }

        ENDCG
    }    
    }
    FallBack "Diffuse"
}
